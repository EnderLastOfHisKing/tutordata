(async function () {

    var color = Chart.helpers.color;
    var scatterChartData = {
        datasets: [{
            label: 'My First dataset',
            borderColor: window.chartColors.red,
            backgroundColor: color(window.chartColors.red).alpha(0.2).rgbString(),
            data: [{
                x: randomScalingFactor(),
                y: randomScalingFactor(),
            }, {
                x: randomScalingFactor(),
                y: randomScalingFactor(),
            }, {
                x: randomScalingFactor(),
                y: randomScalingFactor(),
            }, {
                x: randomScalingFactor(),
                y: randomScalingFactor(),
            }, {
                x: randomScalingFactor(),
                y: randomScalingFactor(),
            }, {
                x: randomScalingFactor(),
                y: randomScalingFactor(),
            }, {
                x: randomScalingFactor(),
                y: randomScalingFactor(),
            }]
        }, {
            label: 'My Second dataset',
            borderColor: window.chartColors.blue,
            backgroundColor: color(window.chartColors.blue).alpha(0.2).rgbString(),
            data: [{
                x: randomScalingFactor(),
                y: randomScalingFactor(),
            }, {
                x: randomScalingFactor(),
                y: randomScalingFactor(),
            }, {
                x: randomScalingFactor(),
                y: randomScalingFactor(),
            }, {
                x: randomScalingFactor(),
                y: randomScalingFactor(),
            }, {
                x: randomScalingFactor(),
                y: randomScalingFactor(),
            }, {
                x: randomScalingFactor(),
                y: randomScalingFactor(),
            }, {
                x: randomScalingFactor(),
                y: randomScalingFactor(),
            }]
        }]
    };


    var ctx = document.getElementById('scattercanvas').getContext('2d');
    Chart.Scatter(ctx, {
        data: scatterChartData,
        options: {
            title: {
                display: true,
                text: 'Chart.js Scatter Chart'
            },
        }
    });


})();
